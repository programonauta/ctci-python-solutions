"""
String Compression: Implement a method to perform basic string compression using the counts
of repeated characters. For example, the string aabcccccaaa would become a2blc5a3 . If the
"compressed" string would not become smaller than the original string, your method should return
the original string. You can assume the string has only uppercase and lowercase letters (a - z).
"""

def convert(s1):
    if len(s1) == 0:
        return s1

    strRet = s1[0]
    prevC = s1[0]
    count = 0
    for c in s1:
        if c != prevC:
            strRet = strRet+str(count)+c
            prevC = c
            count = 1
        else:
            count += 1
    strRet = strRet+str(count)

    if len(strRet) >= len(s1):
        return s1
    else:
        return strRet

print(convert("aabcccccaaa"))
print(convert("abcdef"))
print(convert("aabb"))
print(convert("aaa"))
print(convert("a"))
print(convert(""))
print(convert("xxeefff"))
