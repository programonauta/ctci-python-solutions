# Implement an algorithm to determine if a string has all unique characters. What if you
# cannot use additional data structures?

strName = "Ricardo"

hashTable = {}

for ch in strName:
    if ch in hashTable:
        print("Is not unique")
        exit()
    else:
        hashTable[ch] = 0

print("Is Unique")

