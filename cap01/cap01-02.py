# Given two strings, write a method to decide if one is a permutation of the
# other.

str1 = "god"
str2 = "dog"

list1 = []
list2 = []

list1 = list(str1)
list2 = list(str2)

list1.sort()
list2.sort()

if list1==list2:
    print("Strings are permutable")
else:
    print("Strings are not permutable")