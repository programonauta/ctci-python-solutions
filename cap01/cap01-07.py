""" 
Rotate Matrix: Given an image represented by an NxN matrix, where each pixel in the image is 4
bytes, write a method to rotate the image by 90 degrees. Can you do this in place?
""" 

def rotate(matrix):
    n = len(matrix)
    last = n - 1

    matRev = [[0] * n for i in range(n)]  # empty list of 0s

    for i in range(n):
        for j in range(n):
            matRev[i][j] = matrix[last-j][i]

    return matRev


matrix = [[1,2,3],[4,5,6],[7,8,9]]

print(rotate(matrix))
print(rotate([[1,2,3,4],[5,6,7,8],[9,10,11,12],[13,14,15,16]]))
print(rotate([[1,2,3,4,5],[6,7,8,9,10],[11,12,13,14,15],[16,17,18,19,20],[21,22,23,24,25]]))
