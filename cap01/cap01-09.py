"""
String Rotation: Assume you have a method i5Substring which checks ifone word is a substring
of another. Given two strings, 51 and 52, write code to check if 52 is a rotation of 51 using only one
call to isSubstring (e.g., Uwaterbottleuis a rotation of uerbottlewat U).
"""

def isRotation(s1, s2):
    if len(s1) != len(s2):
        return False
    return s2 in s1 * 2

s1 = "waterbottle"
s2 = "terbottlewa"
print(s1+","+s2+"="+str(isRotation(s1,s2)))
