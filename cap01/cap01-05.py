"""
One Away: There are three types of edits that can be performed on strings: insert a character,
remove a character, or replace a character. Given two strings, write a function to check if they are
one edit (or zero edits) away.
EXAMPLE
pale,  ple  -> true
pales, pale -> true
pale,  bale -> true
pale, bae   -> false
"""

def test(s1, s2):
    if len(s1) == len(s2): # test if only one was edited
        result = oneEdited(s1, s2)
    elif abs(len(s1)-len(s2)) == 1:
        result = oneInserted(s1,s2)
    else:
        result = False

    print(s1+", "+s2+": "+str(result))

def oneEdited(s1, s2):
    edited = False
    for c1, c2 in zip(s1, s2):
        if (c1 != c2):
            if edited:
                return False
            edited = True
    return True

def oneInserted(s1, s2):
    edited = False
    if len(s1) > len(s2):
        sG = s1
        sL = s2
    else:
        sG = s2
        sL = s1
    i, j = 0, 0
    while i < len(sL) and j < len(sG):
        if sL[i] != sG[j]:
            if edited:
                return False
            edited = True
            j += 1
        else:
            i += 1
            j += 1
    return True

test("pale", "pale")
test("", "")
test("pale", "ple")
test("ple", "pale")


test("pale", "ple")
test("ple", "pale")
test("pales", "pale")
test("ples", "pales")
test("pale", "pkle")
test("paleabc", "pleabc")
test("", "d")
test("d", "de")
test("pale", "bale")
test("a", "b")
test("pale", "ble")
test("pale", "bake")
test("pale", "pse")
test("pale", "pas")
test("pas", "pale")
test("pkle", "pable")
test("pal", "palks")
test("palks", "pal")
test("ale", "elas")
