"""
Zero Matrix: Write an algorithm such that if an element in an MxN matrix is 0, its entire row and
column are set to 0.
"""

from re import M


def zeroMatrix(matrix):
    nRows = len(matrix)
    nCols = len(matrix[0])
    colsWithZero = []
    for i in range(nRows):
        rowHasZero = False
        for j in range(nCols):
            if matrix[i][j] == 0:
                rowHasZero = True
                if j not in colsWithZero:
                    colsWithZero.append(j)
            if j in colsWithZero:
                matrix[i][j] = 0
        if rowHasZero:
            for j in range(nCols):
                matrix[i][j] = 0
    return matrix
        
matrix = [[0,1,2],[3,4,5],[6,7,8]]
print(zeroMatrix(matrix))

matrix =    [
                [1, 2, 3, 4, 0],
                [6, 0, 8, 9, 10],
                [11, 12, 13, 14, 15],
                [16, 0, 18, 19, 20],
                [21, 22, 23, 24, 25],
            ]
print(zeroMatrix(matrix))
